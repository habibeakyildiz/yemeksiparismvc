﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YemekSiparis.Startup))]
namespace YemekSiparis
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
