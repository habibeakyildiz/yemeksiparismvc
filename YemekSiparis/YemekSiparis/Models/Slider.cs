﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public partial class Slider
    {
        public int ID { get; set; }
        public byte[] SliderFoto { get; set; }
        public string SliderText { get; set; }
        public Nullable<System.DateTime> BasalngicTarih { get; set; }
        public Nullable<System.DateTime> BitisTarih { get; set; }
    }
}