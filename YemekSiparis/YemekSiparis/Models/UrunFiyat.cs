﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public class UrunFiyat
    {


        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UrunFiyatID { get; set; }
        public int UrunID { get; set; }

        public int BirimID { get; set; }
        [Display(Name = "Fiyat")]
        public decimal fiyat { get; set; }
        public string imagePath { get; set; }


        public virtual Urun Urun { get; set; }
        public virtual Birim Birim { get; set; }

    }
}