﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public class Sepet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SepetID { get; set; }
        public int UrunFiyatID { get; set; }       
        public virtual UrunFiyat UrunFiyat { get; set; }
    }
}