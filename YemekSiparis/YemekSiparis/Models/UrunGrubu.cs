﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public class UrunGrubu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UrunGrubuID { get; set; }
        [Display(Name = "Ürün Grubu")]
        public string UrunGrubuAdi { get; set; }
        public virtual ICollection<Urun> Urunler { get; set; }
    }
}