﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public class RolKullaniciEkle
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}