﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public class Birim
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BirimID { get; set; }
       
        [Display(Name = "Birim Adı")]
        public string BirimAdi { get; set; }
        [Display(Name = "Açıklama")]
        public string birimAciklama { get; set; }
    }
}