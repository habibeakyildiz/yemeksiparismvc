﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public class Siparis
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SiparisID { get; set; }
        public int UrunFiyatID { get; set; }
        [Display(Name = "Ad Soyad")]
        public string adSoyad { get; set; }
        [Display(Name = "Adres")]
        public string adres { get; set; }
        [Display(Name = "Telefon")]
        public string telefon { get; set; }
        public virtual UrunFiyat UrunFiyat { get; set; }
    }
}