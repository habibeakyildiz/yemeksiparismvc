﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YemekSiparis.Models
{
    public class Urun
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UrunID { get; set; }
        public int UrunGrubuID { get; set; }
        [Display(Name = "Ürün Adı")]
        public string UrunAdi { get; set; }
        public virtual UrunGrubu UrunGrubu { get; set; }

    }
}