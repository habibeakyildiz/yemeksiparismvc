﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSiparis.DAL;
using YemekSiparis.Models;

namespace YemekSiparis.Controllers
{
    public class UrunFiyatController : Controller
    {
        private YemekSiparisContext db = new YemekSiparisContext();

        // GET: UrunFiyat
        public ActionResult Index()
        {
            var urunFiyat = db.UrunFiyat.Include(u => u.Birim).Include(u => u.Urun);
            return View(urunFiyat.ToList());
        }

        // GET: UrunFiyat/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunFiyat urunFiyat = db.UrunFiyat.Find(id);
            if (urunFiyat == null)
            {
                return HttpNotFound();
            }
            return View(urunFiyat);
        }

        // GET: UrunFiyat/Create
        public ActionResult Create()
        {
            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi");
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi");
            return View();
        }

        // POST: UrunFiyat/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UrunFiyatID,UrunID,BirimID,fiyat,imagePath")] UrunFiyat urunFiyat)
        {
            if (ModelState.IsValid)
            {
                db.UrunFiyat.Add(urunFiyat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi", urunFiyat.BirimID);
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi", urunFiyat.UrunID);
            return View(urunFiyat);
        }

        // GET: UrunFiyat/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunFiyat urunFiyat = db.UrunFiyat.Find(id);
            if (urunFiyat == null)
            {
                return HttpNotFound();
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi", urunFiyat.BirimID);
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi", urunFiyat.UrunID);
            return View(urunFiyat);
        }

        // POST: UrunFiyat/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UrunFiyatID,UrunID,BirimID,fiyat,imagePath")] UrunFiyat urunFiyat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(urunFiyat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi", urunFiyat.BirimID);
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi", urunFiyat.UrunID);
            return View(urunFiyat);
        }

        // GET: UrunFiyat/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunFiyat urunFiyat = db.UrunFiyat.Find(id);
            if (urunFiyat == null)
            {
                return HttpNotFound();
            }
            return View(urunFiyat);
        }

        // POST: UrunFiyat/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UrunFiyat urunFiyat = db.UrunFiyat.Find(id);
            db.UrunFiyat.Remove(urunFiyat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
