﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSiparis.DAL;
using YemekSiparis.Models;
using System.Data;
using System.Data.Entity;
using System.Net;

namespace YemekSiparis.Controllers
{

    public class HomeController : Controller
    {
        private YemekSiparisContext db = new YemekSiparisContext();


        public ActionResult Index()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            /// obj.Slider = db.Slider.Where(x => (x.BasalngicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList(); obj.Slider = db.Slider.Where(x => (x.BasalngicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList(); obj.Slider = db.Slider.Where(x => (x.BasalngicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
            obj.Slider = db.Slider.ToList();

            return View(obj);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult UrunFiyat()
        {
            var urunFiyat = db.UrunFiyat.Include(u => u.Birim).Include(u => u.Urun);
            return View("Lezzetler", urunFiyat.ToList());
        }

        public ActionResult Lezzetler()
        {
            return View("Lezzetler");
        }
        public ActionResult CreateSiparis11(int UrunFiyatId)
        {

            Siparis siparis = new Siparis { UrunFiyatID = UrunFiyatId };

            siparis = db.Siparis.Add(siparis);
            db.SaveChanges();
            return View("Lezzetler", db.UrunFiyat.ToList());
        }

        public ActionResult Sepetim()
        {
            var sepet = db.Sepet.Include(s => s.UrunFiyat);
            return View(sepet.ToList());
        }
        public ActionResult SepeteEkle(int? id)

        {
            UrunFiyat fiyat = db.UrunFiyat.Find(id);
            Sepet sepet = new Sepet { UrunFiyatID = fiyat.UrunFiyatID };
            db.Sepet.Add(sepet);
            db.SaveChanges();
            var urunFiyat = db.UrunFiyat.Include(u => u.Birim).Include(u => u.Urun);
            return View("Lezzetler", urunFiyat);
        }
        [HttpPost]
        public ActionResult tamamla(Siparis siparis)
        {
            List<Sepet> sepetList = db.Sepet.ToList();
            foreach (var item in sepetList)
            {
                siparis.UrunFiyatID = item.UrunFiyatID;
                db.Siparis.Add(siparis);
                db.Sepet.Remove(item);
                db.SaveChanges();

            }


            var urunFiyat = db.UrunFiyat.Include(u => u.Birim).Include(u => u.Urun);
            return View("Lezzetler", urunFiyat.ToList());

        }

        [HttpPost]
        public ActionResult CompleteSiparis()
        {
            List<Sepet> sepet = db.Sepet.ToList();
            if (sepet != null && sepet.Count > 0)
            {
                return View("CompleteSiparis");
            }
            else
            {
                return View("SepetBos");
            }

        }
    }

    public class AnaSayfaDTO
    {
        public List<Slider> Slider { get; set; }
    }

}