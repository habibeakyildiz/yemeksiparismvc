﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSiparis.DAL;
using YemekSiparis.Models;

namespace YemekSiparis.Controllers { 
   [Authorize(Roles = "Admin")]

    public class YoneticiController : Controller
    {
        private YemekSiparisContext db = new YemekSiparisContext();
        private ApplicationDbContext appDb = new ApplicationDbContext();

        // GET: UrunGrubu
        public ActionResult Index()
        {
            return View();
        }
        #region //UrunGruplari
        public ActionResult UrunGruplari()
        {
            return View(db.UrunGruplari.ToList());
        }
        public ActionResult CreateUrunGruplari()
        {
            return View("UrunGruplariCreate");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUrunGruplari([Bind(Include = "UrunGrubuID,UrunGrubuAdi")] UrunGrubu urunGrubu)
        {
            if (ModelState.IsValid)
            {
                db.UrunGruplari.Add(urunGrubu);
                db.SaveChanges();
                return RedirectToAction("UrunGruplari");
            }

            return View(urunGrubu);
        }
      
        // GET: UrunGrubu/Edit/5
        public ActionResult EditUrunGruplari(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunGrubu urunGrubu = db.UrunGruplari.Find(id);
            if (urunGrubu == null)
            {
                return HttpNotFound();
            }
            return View("UrunGruplariEdit", urunGrubu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUrunGruplari([Bind(Include = "UrunGrubuID,UrunGrubuAdi")] UrunGrubu urunGrubu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(urunGrubu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("UrunGruplari");
            }
            return View("UrunGruplariEdit", urunGrubu);
        }
        public ActionResult DeleteUrunGruplari(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunGrubu urunGrubu = db.UrunGruplari.Find(id);
            if (urunGrubu == null)
            {
                return HttpNotFound();
            }
            return View("UrunGruplariDelete", urunGrubu);
        }

        // POST: UrunGrubu/Delete/5
        [HttpPost, ActionName("DeleteUrunGruplari")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UrunGrubu urunGrubu = db.UrunGruplari.Find(id);
            db.UrunGruplari.Remove(urunGrubu);
            db.SaveChanges();
            return RedirectToAction("UrunGruplari");
        }
        #endregion
        #region //Urunler

        public ActionResult Urunler()
        {
            var urunler = db.Urunler.Include(u => u.UrunGrubu);
            return View("Urunler",urunler.ToList());
        }
        public ActionResult EditUrunler(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urunler.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            ViewBag.UrunGrubuID = new SelectList(db.UrunGruplari, "UrunGrubuID", "UrunGrubuAdi", urun.UrunGrubuID);
            return View("UrunlerEdit", urun);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUrunler([Bind(Include = "UrunID,UrunGrubuID,UrunAdi")] Urun urun)
        {
            if (ModelState.IsValid)
            {
                db.Entry(urun).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Urunler");
            }
            ViewBag.UrunGrubuID = new SelectList(db.UrunGruplari, "UrunGrubuID", "UrunGrubuAdi", urun.UrunGrubuID);
            return View("UrunlerEdit", urun);
        }
        // GET: Urun/Create
        public ActionResult CreateUrunler()
        {
            ViewBag.UrunGrubuID = new SelectList(db.UrunGruplari, "UrunGrubuID", "UrunGrubuAdi");
            return View("UrunlerCreate");
        }

        // POST: Urun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUrunler([Bind(Include = "UrunID,UrunGrubuID,UrunAdi")] Urun urun)
        {
            if (ModelState.IsValid)
            {
                db.Urunler.Add(urun);
                db.SaveChanges();
                return RedirectToAction("Urunler");
            }

            ViewBag.UrunGrubuID = new SelectList(db.UrunGruplari, "UrunGrubuID", "UrunGrubuAdi", urun.UrunGrubuID);
            return View("UrunlerCreate",urun);
        }
        public ActionResult DeleteUrunler(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urunler.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View("UrunlerDelete",urun);
        }

        // POST: Urun/Delete/5
        [HttpPost, ActionName("DeleteUrunler")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedForUrunler(int id)
        {
            Urun urun = db.Urunler.Find(id);
            db.Urunler.Remove(urun);
            db.SaveChanges();
            return RedirectToAction("Urunler");
        }

        #endregion
        #region //Birimler
        public ActionResult Birimler()
        {
            return View(db.Birim.ToList());
        }
        // GET: Birim/Create
        public ActionResult CreateBirimler()
        {
            return View("BirimlerCreate");
        }

        // POST: Birim/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateBirimler([Bind(Include = "BirimID,BirimAdi,birimAciklama")] Birim birim)
        {
            if (ModelState.IsValid)
            {
                db.Birim.Add(birim);
                db.SaveChanges();
                return RedirectToAction("Birimler");
            }

            return View("BirimlerCreate",birim);
        }
        public ActionResult DeleteBirimler(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Birim birim = db.Birim.Find(id);
            if (birim == null)
            {
                return HttpNotFound();
            }
            return View("BirimlerDelete",birim);
        }

        // POST: Birim/Delete/5
        [HttpPost, ActionName("DeleteBirimler")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedForBirimler(int id)
        {
            Birim birim = db.Birim.Find(id);
            db.Birim.Remove(birim);
            db.SaveChanges();
            return RedirectToAction("Birimler");
        }

        #endregion


        #region //UrunFiyat
        public ActionResult UrunFiyat()
        {
            var urunFiyat = db.UrunFiyat.Include(u => u.Birim).Include(u => u.Urun);
            return View(urunFiyat.ToList());
        }
        // GET: UrunFiyat/Create
        public ActionResult CreateUrunFiyat()
        {
            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi");
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi");
            return View("UrunFiyatCreate");
        }

        // POST: UrunFiyat/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUrunFiyat([Bind(Include = "UrunFiyatID,UrunID,BirimID,fiyat,imagePath")] UrunFiyat urunFiyat)
        {
            if (ModelState.IsValid)
            {
                db.UrunFiyat.Add(urunFiyat);
                db.SaveChanges();
                return RedirectToAction("UrunFiyat");
            }

            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi", urunFiyat.BirimID);
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi", urunFiyat.UrunID);
            return View(urunFiyat);
        }
        // GET: UrunFiyat/Edit/5
        public ActionResult EditUrunFiyat(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunFiyat urunFiyat = db.UrunFiyat.Find(id);
            if (urunFiyat == null)
            {
                return HttpNotFound();
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi", urunFiyat.BirimID);
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi", urunFiyat.UrunID);
            return View("UrunFiyatEdit",urunFiyat);
        }

        // POST: UrunFiyat/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUrunFiyat([Bind(Include = "UrunFiyatID,UrunID,BirimID,fiyat,imagePath")] UrunFiyat urunFiyat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(urunFiyat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("UrunFiyat");
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimID", "BirimAdi", urunFiyat.BirimID);
            ViewBag.UrunID = new SelectList(db.Urunler, "UrunID", "UrunAdi", urunFiyat.UrunID);
            return View("UrunFiyatEdit",urunFiyat);
        }
        // GET: UrunFiyat/Delete/5
        public ActionResult DeleteUrunFiyat(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunFiyat urunFiyat = db.UrunFiyat.Find(id);
            if (urunFiyat == null)
            {
                return HttpNotFound();
            }
            return View("UrunFiyatDelete",urunFiyat);
        }

        // POST: UrunFiyat/Delete/5
        [HttpPost, ActionName("DeleteUrunFiyat")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedForUrunFiyat(int id)
        {
            UrunFiyat urunFiyat = db.UrunFiyat.Find(id);
            db.UrunFiyat.Remove(urunFiyat);
            db.SaveChanges();
            return RedirectToAction("UrunFiyat");
        }


        #endregion

        ///Role Management
        #region //Role Management
        public ActionResult Roles()
        {
            var roleStore = new RoleStore<IdentityRole>(appDb);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var model=roleManager.Roles.ToList();          

            return View(model);
        }
        public ActionResult RolesUsers()
        {

            var userStore = new UserStore<ApplicationUser>(appDb);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var model = userManager.Users.ToList();

            return View(model);
        }

        public ActionResult RolEkle()
        {
            return View();
        }
        public ActionResult RolKullaniciEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RolEkle(RolEkle rol )
        {
            var roleStore = new RoleStore<IdentityRole>(appDb);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            if (!roleManager.RoleExists(rol.RolAdi))
            {
                roleManager.Create(new IdentityRole(rol.RolAdi));
            }
            return RedirectToAction("Roles");
        }
        [HttpPost]
        public ActionResult RolKullaniciEkle(RolKullaniciEkle model)
        {

            var roleStore = new RoleStore<IdentityRole>(appDb);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var userStore = new UserStore<ApplicationUser>(appDb);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var user = userManager.FindByName(model.KullaniciAdi);
            if (!userManager.IsInRole(user.Id, model.RolAdi))
            {
                userManager.AddToRole(user.Id, model.RolAdi);
            }
           
            return RedirectToAction("Roles");

        }
        #endregion


        ///Sepet

        public ActionResult EkleSepet(int UrunFiyatID)
        {

           
            /// Sepet satis = new Sepet();
            /// satis.kitapKodu = kitapKodu;
            /// satis.kullaniciId = Convert.ToInt32(Session["kullaniciId"]);
            /// if (ModelState.IsValid)
            /// {
            /// db.Sepets.Add(satis);
            /// db.SaveChanges();
            /// return RedirectToAction("Index");
            ///}

            return View("Index", "Home");
            
        }



    }
   
}