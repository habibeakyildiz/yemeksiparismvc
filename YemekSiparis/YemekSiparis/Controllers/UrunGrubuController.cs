﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSiparis.DAL;
using YemekSiparis.Models;

namespace YemekSiparis.Controllers
{
    public class UrunGrubuController : Controller
    {
        private YemekSiparisContext db = new YemekSiparisContext();

        // GET: UrunGrubu
        public ActionResult Index()
        {
            return View(db.UrunGruplari.ToList());
        }

        // GET: UrunGrubu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunGrubu urunGrubu = db.UrunGruplari.Find(id);
            if (urunGrubu == null)
            {
                return HttpNotFound();
            }
            return View(urunGrubu);
        }

        // GET: UrunGrubu/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UrunGrubu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UrunGrubuID,UrunGrubuAdi")] UrunGrubu urunGrubu)
        {
            if (ModelState.IsValid)
            {
                db.UrunGruplari.Add(urunGrubu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(urunGrubu);
        }

        // GET: UrunGrubu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunGrubu urunGrubu = db.UrunGruplari.Find(id);
            if (urunGrubu == null)
            {
                return HttpNotFound();
            }
            return View(urunGrubu);
        }

        // POST: UrunGrubu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UrunGrubuID,UrunGrubuAdi")] UrunGrubu urunGrubu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(urunGrubu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(urunGrubu);
        }

        // GET: UrunGrubu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UrunGrubu urunGrubu = db.UrunGruplari.Find(id);
            if (urunGrubu == null)
            {
                return HttpNotFound();
            }
            return View(urunGrubu);
        }

        // POST: UrunGrubu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UrunGrubu urunGrubu = db.UrunGruplari.Find(id);
            db.UrunGruplari.Remove(urunGrubu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
