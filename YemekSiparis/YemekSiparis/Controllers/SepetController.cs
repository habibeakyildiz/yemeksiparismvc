﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSiparis.DAL;
using YemekSiparis.Models;

namespace YemekSiparis.Controllers
{
    public class SepetController : Controller
    {
        private YemekSiparisContext db = new YemekSiparisContext();

        // GET: Sepet
        public ActionResult Index()
        {
            var sepet = db.Sepet.Include(s => s.UrunFiyat);
            return View(sepet.ToList());
        }

        // GET: Sepet/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sepet sepet = db.Sepet.Find(id);
            if (sepet == null)
            {
                return HttpNotFound();
            }
            return View(sepet);
        }

        // GET: Sepet/Create
        public ActionResult Create()
        {
            ViewBag.UrunFiyatID = new SelectList(db.UrunFiyat, "UrunFiyatID", "imagePath");
            return View();
        }

        // POST: Sepet/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SepetID,UrunFiyatID")] Sepet sepet)
        {
            if (ModelState.IsValid)
            {
                db.Sepet.Add(sepet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UrunFiyatID = new SelectList(db.UrunFiyat, "UrunFiyatID", "imagePath", sepet.UrunFiyatID);
            return View(sepet);
        }

        // GET: Sepet/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sepet sepet = db.Sepet.Find(id);
            if (sepet == null)
            {
                return HttpNotFound();
            }
            ViewBag.UrunFiyatID = new SelectList(db.UrunFiyat, "UrunFiyatID", "imagePath", sepet.UrunFiyatID);
            return View(sepet);
        }

        // POST: Sepet/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SepetID,UrunFiyatID")] Sepet sepet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sepet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UrunFiyatID = new SelectList(db.UrunFiyat, "UrunFiyatID", "imagePath", sepet.UrunFiyatID);
            return View(sepet);
        }

        // GET: Sepet/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sepet sepet = db.Sepet.Find(id);
            if (sepet == null)
            {
                return HttpNotFound();
            }
            return View(sepet);
        }

        // POST: Sepet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sepet sepet = db.Sepet.Find(id);
            db.Sepet.Remove(sepet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
