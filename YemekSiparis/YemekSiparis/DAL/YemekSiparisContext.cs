﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using YemekSiparis.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace YemekSiparis.DAL
{
    public class YemekSiparisContext:DbContext
    {
        public YemekSiparisContext() : base("YemekSiparisContext") { }
        

        public DbSet<Urun> Urunler { get; set; }
        public DbSet<UrunGrubu> UrunGruplari { get; set; }
        public DbSet<Slider> Slider { get; set; }
        public DbSet<Birim> Birim { get; set; }

        public DbSet<UrunFiyat> UrunFiyat { get; set; }
        public DbSet<Siparis> Siparis { get; set; }
        public DbSet<Sepet> Sepet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}